angular.module('starter.services', [])


.factory('Dictionary', function() {
  

  if (localStorage.getItem("dictionaryData") === null) {
        
        console.log('Nothing in localStorage, making new dictionary array');    
    
    /*TODO: Read from json file.*/

     var dictionary = [
    {
        "id": 0,
        "en": "anus",
        "jp": "ケツ穴",
        "reading": "けつあな",
        "grammar": "名詞",
        "ex1eng": "My anus is sore.",
        "ex1jp": "私のケツの穴が痛い。",
        "level": 10,
        "fav": false
    },
    {
        "id": 1,
        "en": "arse, ass",
        "jp": "ケツ、おしり",
        "reading": "けつ、おしり",
        "grammar": "名詞",
        "ex1eng": "You have a nice arse.",
        "ex1jp": "君は良いケツがるよ。",
        "level": 10,
        "fav": false
    },
    {
        "id": 2,
        "en": "arsehole, asshole",
        "jp": "ケツ穴、アホ、てめぇ、感じ悪い",
        "reading": "けつあな、あほ、てめえ、てめぇー、テメェ",
        "grammar": "名詞",
        "ex1eng": "Fuck you, arsehole.",
        "ex1jp": "死ね、アホよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 3,
        "en": "ass-hat, asshat",
        "jp": "アホ",
        "reading": "あほ",
        "grammar": "名詞",
        "ex1eng": "You are an ass-hat!",
        "ex1jp": "お前はアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 4,
        "en": "ass-pirate",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an ass-pirate.",
        "ex1jp": "彼はゲイだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 5,
        "en": "assbandit",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an assbandit.",
        "ex1jp": "彼はゲイだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 6,
        "en": "assbanger",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an assbanger.",
        "ex1jp": "彼はゲイだよ！",
        "level": 70,
        "fav": false
    },
    {
        "id": 7,
        "en": "assfucker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an assfucker.",
        "ex1jp": "彼はゲイだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 8,
        "en": "asswipe",
        "jp": "アホ、悪い人",
        "reading": "あほ、わるいひと",
        "grammar": "名詞",
        "ex1eng": "He is an asshole.",
        "ex1jp": "彼はアホだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 9,
        "en": "asslick, asslicker",
        "alternate": "ass lick, ass licker",
        "jp": "ごますり人間、おべっか使い",
        "reading": "ごますり",
        "grammar": "動詞",
        "ex1eng": "I have to asslick at work.",
        "ex1jp": "私は仕事で胡麻擂りをしないといけいない",
        "level": 10,
        "fav": false
    },
    {
        "id": 10,
        "en": "ass kisser, kissass",
        "alternate": "kiss ass",
        "jp": "胡麻擂りする人",
        "reading": "ごますりするひと",
        "grammar": "名詞",
        "ex1eng": "He is an asslicker.",
        "ex1jp": "彼は胡麻擂り人だよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 11,
        "en": "assmuncher",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an assmuncher.",
        "ex1jp": "彼はゲイだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 12,
        "en": "asspirate",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an ass pirate!",
        "ex1jp": "彼がゲイだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 13,
        "en": "axewound, pussy",
        "jp": "まんこ、マンコ",
        "reading": "まんこ",
        "grammar": "名詞",
        "ex1eng": "Look at that pussy!",
        "ex1jp": "あのマンコ見てよ！",
        "level": 40,
        "fav": false
    },
    {
        "id": 14,
        "en": "badass",
        "jp": "凄く良い、最高",
        "reading": "さいこう",
        "grammar": "形容詞",
        "ex1eng": "That car is badass!",
        "ex1jp": "あの車最高でしょう！",
        "level": 1,
        "fav": false
    },
    {
        "id": 15,
        "en": "bastard",
        "jp": "糞垂れ、アホ (男)",
        "reading": "くそたれ",
        "grammar": "名詞",
        "ex1eng": "My ex-boyfriend was a bastard.",
        "ex1jp": "私の元彼はめっちゃ悪い人だった。",
        "level": 10,
        "fav": false
    },
    {
        "id": 16,
        "en": "bitch",
        "jp": "女、あばずれ、やりマン、弱い男、文句する",
        "reading": "おんな、あばずれ、やりまん、よわいおとこ",
        "grammar": "名詞",
        "ex1eng": "She is a bitch!",
        "ex1jp": "彼女はあばずれだよ！",
        "grammar2": "動詞",
        "ex2eng": "Tom bitches everyday!",
        "ex2jp": "トムは毎日文句がいっぱいあるよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 17,
        "en": "bitchy",
        "jp": "文句が多い人",
        "reading": "もんくがおおいひと",
        "grammar": "形容詞",
        "ex1eng": "She is bitchy!",
        "ex1jp": "彼女は文句いっぱいあるタイプ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 18,
        "en": "blow job, blowjob",
        "alternate": "blow job",
        "jp": "フェラ、ふぇら",
        "reading": "フェラ、ふぇら",
        "grammar": "名詞",
        "ex1eng": "I love a good blowjob!",
        "ex1jp": "俺は良いフェラされるのが大好き！",
        "grammar2": "動詞",
        "ex2eng": "I gave Mike a blowjob last night.",
        "ex2jp": "私は昨夜、マイクにフェラをしてあげました。",
        "level": 30,
        "fav": false
    },
    {
        "id": 19,
        "en": "bollocks, balls, nuts",
        "alternate": "bollox",
        "jp": "金玉",
        "reading": "きんたま",
        "grammar": "名詞",
        "ex1eng": "Lick my balls!",
        "ex1jp": "金玉をなめろ",
        "level": 10,
        "fav": false
    },
    {
        "id": 20,
        "en": "booty",
        "jp": "ケツ、おしり",
        "reading": "けつ",
        "grammar": "名詞",
        "ex1eng": "I like big bootys!",
        "ex1jp": "大きいおしりが大好き。",
        "level": 1,
        "fav": false
    },
    {
        "id": 21,
        "en": "boner",
        "jp": "ギンギン、立ってる、エレクション",
        "reading": "ギンギン、立ってる、エレクション",
        "grammar": "名詞",
        "ex1eng": "I have a boner.",
        "ex1jp": "俺のちんちんが立ってるよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 22,
        "en": "bullshit",
        "jp": "嘘、ウソ、悪い",
        "reading": "うそ、わるい",
        "grammar": "形容詞",
        "ex1eng": "This restaurant is bullshit.",
        "ex1jp": "この店がマジ悪いよ！",
        "grammar2": "動詞",
        "ex2eng": "I bullshit at work everyday.",
        "ex2jp": "俺は毎日仕事で嘘をつける！",
        "level": 10,
        "fav": false
    },
    {
        "id": 23,
        "en": "buttfucker",
        "alternate": "butt fucker, buttfucka",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "Are you a buttfucker?",
        "ex1jp": "お前はおかまですか？",
        "level": 70,
        "fav": false
    },
    {
        "id": 24,
        "en": "cameltoe",
        "alternate": "camel toe",
        "jp": "メコ筋",
        "reading": "めこすじ",
        "grammar": "名詞",
        "ex1eng": "I can see your cameltoe!",
        "ex1jp": "あなたのメコ筋を見えるよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 25,
        "en": "carpetmuncher",
        "alternate": "carpet muncher",
        "jp": "レズ",
        "reading": "れず",
        "grammar": "名詞",
        "ex1eng": "Yuki is a carpet muncher.",
        "ex1jp": "ユキさんはレズです。",
        "level": 10,
        "fav": false
    },
    {
        "id": 26,
        "en": "chubby-chaser",
        "alternate": "chubby chaser",
        "jp": "デブセン",
        "reading": "でぶせん",
        "grammar": "名詞",
        "ex1eng": "He's a chubby-chaser.",
        "ex1jp": "あいつはデブセンだよ",
        "level": 10,
        "fav": false
    },
    {
        "id": 27,
        "en": "clit, clitioris",
        "jp": "くり、まめ、クリトリス",
        "reading": "くり、まめ、クリトリス、マメ、豆",
        "grammar": "名詞",
        "ex1eng": "You have a beautiful clit.",
        "ex1jp": "君のクリは美しい。",
        "level": 10,
        "fav": false
    },
    {
        "id": 28,
        "en": "clusterfuck",
        "alternate": "cluster fuck",
        "jp": "ダメの事、台無し",
        "reading": "だめのこと、だいなし",
        "grammar": "名詞",
        "ex1eng": "Yesterday was a cluster fuck",
        "ex1jp": "昨日は台無しだった。",
        "level": 10,
        "fav": false
    },
    {
        "id": 29,
        "en": "cock, penis",
        "jp": "ちんこ、ちんぽ、ペニス、アホ",
        "reading": "ちんこ、ちんぽ、ぺにす、あほ",
        "grammar": "名詞",
        "ex1eng": "Suck my cock.",
        "ex1jp": "俺のちんぽをなめて",
        "grammar2": "名詞",
        "ex2eng": "He is a cock.",
        "ex2jp": "彼はアホです。",
        "level": 10,
        "fav": false
    },
    {
        "id": 30,
        "en": "cocksmoker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an cock smoker.",
        "ex1jp": "そいつはゲイよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 31,
        "en": "cocksucker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He is an cocksucker.",
        "ex1jp": "そいつはゲイよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 32,
        "en": "coochie, cooch",
        "alternate": "coochy, kooch, koochie, koochy",
        "jp": "マンコ",
        "reading": "マンコ、まんこ",
        "grammar": "名詞",
        "ex1eng": "Your cooch tastes good!",
        "ex1jp": "君のマンコが美味しいよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 33,
        "en": "cum, semen, jizz, spoof, spunk",
        "jp": "いく、イク、精子",
        "reading": "いく、せいし",
        "grammar": "名詞",
        "ex1eng": "Drink all that cum.",
        "ex1jp": "精子全飲みな",
        "grammar2": "動詞",
        "ex2eng": "Can I cum inside you?",
        "ex2jp": "中に出しても良い？",
        "level": 10,
        "fav": false
    },
    {
        "id": 34,
        "en": "cumdumpster",
        "jp": "売春婦、やりマン",
        "reading": "ばいしゅんふ、やりまん",
        "grammar": "名詞",
        "ex1eng": "Yukiko is a cumdumster.",
        "ex1jp": "ユキコちゃんはやりマンだよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 35,
        "en": "cunnilingus",
        "jp": "クンニ、クンニリングス、ハーモニカ",
        "reading": "クンニ、くんに",
        "grammar": "名詞",
        "ex1eng": "I love cunninglingus.",
        "ex1jp": "早くクンニしてくれ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 36,
        "en": "cunt",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "Her cunt tastes so good.",
        "ex1jp": "彼女のマンコがめっちゃおいしい！",
        "level": 70,
        "fav": false
    },
    {
        "id": 37,
        "en": "cunthole",
        "jp": "まんこ",
        "reading": "まんこ、マンコ、あほ、アホ",
        "grammar": "名詞",
        "ex1eng": "I put my dick in her cunthole.",
        "ex1jp": "彼女のマンコにいれちゃった！",
        "level": 70,
        "fav": false
    },
    {
        "id": 38,
        "en": "cuntlicker",
        "jp": "レズ",
        "reading": "レズ",
        "grammar": "名詞",
        "ex1eng": "Miki is a cuntlicker.",
        "ex1jp": "みきちゃんはれずだよ！",
        "level": 70,
        "fav": false
    },
    {
        "id": 39,
        "en": "damn, dayum",
        "jp": "ちくしょう、畜生",
        "reading": "ちくしょ",
        "grammar": "フレーズ",
        "ex1eng": "Damn! I made a mistake.",
        "ex1jp": "ちくしょ！失敗した！",
        "level": 10,
        "fav": false
    },
    {
        "id": 40,
        "en": "dick, penis",
        "jp": "ちんぽ、ちんちん、ちんこ、ペニス、アホ、",
        "reading": "チンポ、チンチン、チンコ、ペニス",
        "grammar": "名詞",
        "ex1eng": "I have a big dick.",
        "ex1jp": "俺のちんぽでかいです。",
        "grammar2": "名詞",
        "ex2eng": "You're a dick!",
        "ex2jp": "お前はアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 41,
        "en": "dick-sneeze",
        "jp": "いく",
        "reading": "イク",
        "grammar": "名詞",
        "ex1eng": "She likes a good dick-sneeze.",
        "ex1jp": "彼女はいくのが好きです。",
        "grammar2": "動詞",
        "ex2eng": "I wanna dick-sneeze on her face.",
        "ex2jp": "俺は彼女の顔に出したい！",
        "level": 10,
        "fav": false
    },
    {
        "id": 42,
        "en": "dickface",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "What a dickface!",
        "ex1jp": "なんてあほやね",
        "level": 10,
        "fav": false
    },
    {
        "id": 43,
        "en": "dickhead",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "What a dickhead!",
        "ex1jp": "なんてあほやね",
        "level": 10,
        "fav": false
    },
    {
        "id": 44,
        "en": "dickhole",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "What a dickhead!",
        "ex1jp": "なんてあほやね",
        "level": 10,
        "fav": false
    },
    {
        "id": 45,
        "en": "dickjuice",
        "jp": "いく、イク、精子",
        "reading": "いく、せいし",
        "grammar": "名詞",
        "ex1eng": "Drink all that dickjuice.",
        "ex1jp": "精子全飲みな",
        "level": 30,
        "fav": false
    },
    {
        "id": 46,
        "en": "dickmilk",
        "jp": "いく、イク、精子",
        "reading": "いく、せいし",
        "grammar": "名詞",
        "ex1eng": "Drink all that dickmilk.",
        "ex1jp": "精子全部飲みな",
        "level": 30,
        "fav": false
    },
    {
        "id": 47,
        "en": "dickwad",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "Taro is a dickwad!",
        "ex1jp": "ダイスケ君はアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 48,
        "en": "dickweed",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "Daisuke is a dickweed!",
        "ex1jp": "ダイスケ君はアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 49,
        "en": "dyke, dike",
        "jp": "レズ",
        "reading": "れず",
        "grammar": "名詞",
        "ex1eng": "Yuki is a dike.",
        "ex1jp": "ユキさんはレズです。",
        "level": 10,
        "fav": false
    },
    {
        "id": 50,
        "en": "dildo",
        "jp": "ヴァイブ、うなぎ、おもちゃ",
        "reading": "ゔぁいぶ",
        "grammar": "名詞",
        "ex1eng": "That dildo is massive!!",
        "ex1jp": "あのヴァイブデカイゼ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 51,
        "en": "dipshit",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "Fuck you, dipshit!",
        "ex1jp": "死ね、馬鹿野郎よ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 52,
        "en": "doochbag, douchebag",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "He was a major douchebag.",
        "ex1jp": "彼はマジアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 53,
        "en": "dumass, dumb ass, dumbass",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "Move, dumbass!",
        "ex1jp": "どけお前。",
        "level": 10,
        "fav": false
    },
    {
        "id": 54,
        "en": "dumbshit",
        "alternate": "dumb shit, dumshit",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "He was a major douchebag.",
        "ex1jp": "彼はマジアホだよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 55,
        "en": "dumbfuck",
        "alternate": "dumb fuck",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "You're such a dumbass sometimes.",
        "ex1jp": "お前はたまにバカよね。",
        "level": 10,
        "fav": false
    },
    {
        "id": 56,
        "en": "fag",
        "jp": "たばこ",
        "reading": "タバコ",
        "grammar": "名詞",
        "ex1eng": "Can I have a fag?",
        "ex1jp": "タバコ一本ちょうだい",
        "level": 10,
        "fav": false
    },
    {
        "id": 57,
        "en": "fag, faggot",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "I think Tom is a fag.",
        "ex1jp": "彼はゲイだと思う",
        "level": 30,
        "fav": false
    },
    {
        "id": 58,
        "en": "fatass, fat ass",
        "jp": "デブな人",
        "reading": "でぶ、デブ",
        "grammar": "形容詞",
        "ex1eng": "Hurry up, fatass!",
        "ex1jp": "おいデブ、急げ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 59,
        "en": "fuck",
        "jp": "だく、エッチをする、畜生、やる、ファック",
        "reading": "ちくしょう、ちくしょ、ふぁっく",
        "grammar": "動詞",
        "ex1eng": "I want to fuck Keiko read bad!",
        "ex1jp": "マジでけいこちゃんだきてぇ",
        "grammar2": "フレーズ",
        "ex2eng": "Fuck! I lost my wallet.",
        "ex2jp": "ちくしょう！財布無くした！",
        "level": 30,
        "fav": false
    },
    {
        "id": 60,
        "en": "fuckin, fucking, fuckin'",
        "jp": "なんて〜、くそ",
        "reading": "クソ、ナンテ",
        "grammar": "副詞",
        "ex1eng": "This is fuckin' delicious!",
        "ex1jp": "これはクソうまいぜ",
        "level": 30,
        "fav": false
    },
    {
        "id": 61,
        "en": "fuck up",
        "jp": "黙れ！",
        "reading": "だまれ、ダマレ",
        "grammar": "フレーズ",
        "ex1eng": "Fuck up, bitch!",
        "ex1jp": "黙れお前！",
        "level": 80,
        "fav": false
    },
    {
        "id": 62,
        "en": "fuck you",
        "jp": "死ね！",
        "reading": "しね、シネ",
        "grammar": "フレーズ",
        "ex1eng": "Fuck you!",
        "ex1jp": "死ね、てめバカヤロウ！",
        "level": 80,
        "fav": false
    },
    {
        "id": 63,
        "en": "fuckass",
        "jp": "アホ，あほ、ばか、馬鹿野郎",
        "reading": "あほ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "You're such a fucker sometimes.",
        "ex1jp": "お前はたまにバカよね。",
        "level": 50,
        "fav": false
    },
    {
        "id": 64,
        "en": "fucker",
        "jp": "アホ，あほ、ばか、馬鹿野郎",
        "reading": "あほ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "You're a fuckin fucker.",
        "ex1jp": "お前はマジアホよね。",
        "level": 70,
        "fav": false
    },
    {
        "id": 65,
        "en": "fuckface",
        "jp": "アホ，あほ、ばか、バカ、馬鹿野郎",
        "reading": "あほ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "Fuck up, bitch!",
        "ex1jp": "お前はたまにバカよね。",
        "level": 70,
        "fav": false
    },
    {
        "id": 66,
        "en": "fuckoff",
        "jp": "死ね、消えろ",
        "reading": "しね、シネ、きえろ",
        "grammar": "フレーズ",
        "ex1eng": "Fuck off, asshole.",
        "ex1jp": "死ね、てめぇ！",
        "level": 80,
        "fav": false
    },
    {
        "id": 67,
        "en": "fucktard, fuckwad, fuckwit, fuckwitt",
        "jp": "アホ，あほ、ばか、馬鹿野郎",
        "reading": "あほ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "You're such a fucktard sometimes.",
        "ex1jp": "お前はたまにバカよね。",
        "level": 70,
        "fav": false
    },
    {
        "id": 68,
        "en": "fuckup",
        "jp": "ダメな人間",
        "reading": "だめ、",
        "grammar": "名詞",
        "ex1eng": "He can't do anything, he's just a fuckup.",
        "ex1jp": "あいつはダメな人間だよ！何も出来ん",
        "level": 70,
        "fav": false
    },
    {
        "id": 69,
        "en": "fudgepacker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "He's a fudgepacker.",
        "ex1jp": "あいつはゲイだよ！",
        "level": 70,
        "fav": false
    },
    {
        "id": 70,
        "en": "gay, gaylord, homo, homosexual",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "形容詞",
        "ex1eng": "Didn't you know he's gay?",
        "ex1jp": "あいつはゲイのが知らなかった？",
        "grammar2": "名詞",
        "ex2eng": "You're such a gaylord!",
        "ex2jp": "お前はゲイだ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 71,
        "en": "goddamn, goddamnit, godammit",
        "jp": "ちくしょう、畜生",
        "reading": "ちくしょ",
        "grammar": "フレーズ",
        "ex1eng": "Goddammit! I made a mistake.",
        "ex1jp": "ちくしょ！だい失敗だ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 72,
        "en": "gook",
        "jp": "中国人",
        "reading": "ちゅうごくじん",
        "grammar": "名詞",
        "level": 100,
        "fav": false
    },
    {
        "id": 73,
        "en": "guido",
        "jp": "イタリア人",
        "reading": "いたりあじん",
        "grammar": "名詞",
        "level": 100,
        "fav": false
    },
    {
        "id": 74,
        "en": "handjob",
        "jp": "手コキ",
        "reading": "てこき、てコキ",
        "grammar": "名詞",
        "ex1eng": "She gives a good handjob.",
        "ex1jp": "彼女は手コキうまい",
        "level": 20,
        "fav": false
    },
    {
        "id": 75,
        "en": "hard on",
        "jp": "ギンギン、立ってる、エレクション",
        "reading": "ギンギン、立ってる、エレクション",
        "grammar": "名詞",
        "ex1eng": "Mai gives me a hard on.",
        "ex1jp": "マイちゃんを見るとびんびんなっちゃう。",
        "level": 10,
        "fav": false
    },
    {
        "id": 76,
        "en": "ho, hoe",
        "jp": "やりまん",
        "reading": "やりマン",
        "grammar": "名詞",
        "ex1eng": "Wow, that girl is a ho.",
        "ex1jp": "すげぇ、あの女はマジやりマンだね。",
        "level": 30,
        "fav": false
    },
    {
        "id": 77,
        "en": "hell yeah",
        "jp": "当たり前よ",
        "reading": "あたりまえ",
        "grammar": "フレーズ",
        "ex1eng": "You want a beer? -Hell yeah!",
        "ex1jp": "ビール欲しい？-当たり前だよ！",
        "level": 0,
        "fav": false
    },
    {
        "id": 78,
        "en": "honkey",
        "jp": "白人",
        "reading": "はくじん、ハクジン",
        "grammar": "名詞",
        "level": 100,
        "fav": false
    },
    {
        "id": 79,
        "en": "hump",
        "jp": "えち、だく",
        "reading": "ダク、エチ",
        "grammar": "動詞",
        "ex1eng": "I was humping her all night.",
        "ex1jp": "朝までエチしてたよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 80,
        "en": "jackass",
        "jp": "アホ、てめぇ、感じ悪い",
        "reading": "あほ、てめえ、てめぇー、テメェ",
        "grammar": "名詞",
        "ex1eng": "Don't worry, he's just a jackass.",
        "ex1jp": "ドンマイ、あいつはバカだよ！",
        "level": 15,
        "fav": false
    },
    {
        "id": 81,
        "en": "jap",
        "jp": "日本人",
        "reading": "にほんじん",
        "grammar": "名詞",
        "level": 100,
        "fav": false
    },
    {
        "id": 82,
        "en": "jerk off, masturbate, wank",
        "jp": "オナニ",
        "reading": "オナニー、おなに",
        "grammar": "動詞",
        "ex1eng": "Do you wank everyday?",
        "ex1jp": "毎日オナニーするの？",
        "level": 10,
        "fav": false
    },
    {
        "id": 83,
        "en": "lame",
        "jp": "ださい、つまらない、退屈",
        "reading": "ダサイ、つまらん、つまらない、たいくつ",
        "grammar": "形容詞",
        "ex1eng": "This club is lame.",
        "ex1jp": "このクラブダサイなー",
        "level": 1,
        "fav": false
    },
    {
        "id": 84,
        "en": "lameass",
        "jp": "ダメな人間、ダサイ人間、つまらない",
        "reading": "だめ、",
        "grammar": "名詞",
        "ex1eng": "Let's go, this dude is a lameass.",
        "ex1jp": "行こうよ！こいつがつまらない。",
        "level": 8,
        "fav": false
    },
    {
        "id": 85,
        "en": "lardass, lard ass",
        "jp": "デブな人",
        "reading": "でぶ、デブ",
        "grammar": "形容詞",
        "ex1eng": "Hurry up, lardass!",
        "ex1jp": "急げデブ！",
        "level": 15,
        "fav": false
    },
    {
        "id": 86,
        "en": "lesbian",
        "jp": "レズ",
        "reading": "れず",
        "grammar": "名詞",
        "ex1eng": "There are many lesbians at my school.",
        "ex1jp": "私の学校にたくさんレズがいる。",
        "level": 1,
        "fav": false
    },
    {
        "id": 87,
        "en": "lesbo, lezzie",
        "jp": "レズ",
        "reading": "れず",
        "grammar": "名詞",
        "ex1eng": "Yuki is a carpet muncher.",
        "ex1jp": "ユキさんはレズです。",
        "level": 50,
        "fav": false
    },
    {
        "id": 88,
        "en": "minge, muff",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "I haven't seen a muff in a long time.",
        "ex1jp": "久しぶりにマンコ見てません。",
        "level": 25,
        "fav": false
    },
    {
        "id": 89,
        "en": "morning wood",
        "jp": "朝立ち",
        "reading": "あさたち",
        "grammar": "名詞",
        "ex1eng": "I have morning wood everyday.",
        "ex1jp": "俺は毎日朝立ちだよ。",
        "level": 5,
        "fav": false
    },
    {
        "id": 90,
        "en": "motherfucker, muthafucker,mothafucka",
        "jp": "アホ、てめぇ、感じ悪い",
        "reading": "あほ、てめえ、てめぇー、テメェ",
        "grammar": "名詞",
        "ex1eng": "He's a real muthafucker.",
        "ex1jp": "あいつはマジアホだよ！",
        "level": 90,
        "fav": false
    },
    {
        "id": 91,
        "en": "muffdiver",
        "jp": "レズ",
        "reading": "れず",
        "grammar": "名詞",
        "ex1eng": "Did you hear? Miki's a muffdiver.",
        "ex1jp": "ね、聞いた？みきちゃんはレズだよ。",
        "level": 60,
        "fav": false
    },
    {
        "id": 92,
        "en": "nut sack, nutsack, ballsack, ballsax",
        "jp": "陰囊、陰嚢",
        "reading": "ふぐり、フグリ、いんのう",
        "grammar": "名詞",
        "ex1eng": "Ouch! I cut my nutsack!",
        "ex1jp": "痛ぇ！俺のフグリ切った！",
        "level": 5,
        "fav": false
    },
    {
        "id": 93,
        "en": "pecker",
        "jp": "ちんぽ、ちんちん、ちんこ、ペニス",
        "reading": "チンポ、チンチン、チンコ、ペニス",
        "grammar": "名詞",
        "ex1eng": "I have a small pecker.",
        "ex1jp": "俺短チンだよ。",
        "level": 1,
        "fav": false
    },
    {
        "id": 94,
        "en": "peckerhead",
        "jp": "アホ、てめぇ、感じ悪い",
        "reading": "あほ、てめえ、てめぇー、テメェ",
        "grammar": "名詞",
        "ex1eng": "Fuck you, peckerhead.",
        "ex1jp": "死ね、アホよ！",
        "level": 1,
        "fav": false
    },
    {
        "id": 95,
        "en": "polesmoker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "I think Tom is a polesmoker.",
        "ex1jp": "彼はゲイだと思う",
        "level": 60,
        "fav": false
    },
    {
        "id": 96,
        "en": "poon, poonani, poonany, poontang",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "I'm looking for poonani.",
        "ex1jp": "マンコを探しています。",
        "level": 25,
        "fav": false
    },
    {
        "id": 97,
        "en": "prick, penis",
        "jp": "ちんぽ、ちんちん、ちんこ、ペニス、アホ、",
        "reading": "チンポ、チンチン、チンコ、ペニス",
        "grammar": "名詞",
        "ex1eng": "I have a big dick.",
        "ex1jp": "俺のちんぽでかいです。",
        "grammar2": "名詞",
        "ex2eng": "Mike is a prick!",
        "ex2jp": "マイクは感じ悪！",
        "level": 10,
        "fav": false
    },
    {
        "id": 98,
        "en": "queef",
        "jp": "おなら(女)",
        "reading": "オナラ",
        "grammar": "動詞",
        "ex1eng": "Shit! She queefed in front of everyone.",
        "ex1jp": "やべー！彼女は皆の前おならした",
        "grammar2": "名詞",
        "ex2eng": "Her queefs smell sweet.",
        "ex2jp": "彼女のおならは良い香りがする",
        "level": 5,
        "fav": false
    },
    {
        "id": 99,
        "en": "queer",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "Didn't you know he's a queer?",
        "ex1jp": "あいつはゲイのが知らなかった？",
        "grammar2": "形容詞",
        "ex2eng": "He seems kinda queer.",
        "ex2jp": "彼はちょっとおかまみたい",
        "level": 5,
        "fav": false
    },
    {
        "id": 100,
        "en": "schlong, shlong",
        "jp": "ちんこ、ちんぽ、ペニス",
        "reading": "ちんこ、ちんぽ、ぺにす",
        "grammar": "名詞",
        "ex1eng": "How big was his schlong?",
        "ex1jp": "彼のちんこってどのくらいだった？",
        "level": 5,
        "fav": false
    },
    {
        "id": 101,
        "en": "scrote, scrotum",
        "jp": "陰囊、陰嚢",
        "reading": "ふぐり、フグリ、いんのう",
        "grammar": "名詞",
        "ex1eng": "Ouch! I cut my scrotes!",
        "ex1jp": "痛ぇ！俺のフグリ切った！",
        "level": 1,
        "fav": false
    },
    {
        "id": 102,
        "en": "shit",
        "jp": "うんこ、くそ、ちくしょう、ヤバイ",
        "reading": "クゾ、畜生、やべー、やべ、やば",
        "grammar": "フレーズ",
        "ex1eng": "Shit! I'm late!",
        "ex1jp": "やべー、遅刻だ！",
        "grammar2": "名詞",
        "ex2eng": "Is that shit over there?",
        "ex2jp": "へ？！あっちはうんこなの？",
        "grammar3": "動詞",
        "ex3en": "I want to shit real bad!",
        "ex3jp": "マジうんこしたいよ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 103,
        "en": "shithead, shitbag",
        "jp": "アホ，あほ、ばか、てめ、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう、てめぇ、てめー、テメェ",
        "grammar": "形容詞",
        "ex1eng": "Come on then, shithead!",
        "ex1jp": "かかってこいてめぇ！",
        "level": 40,
        "fav": false
    },
    {
        "id": 104,
        "en": "shitbrains, shit for brains",
        "jp": "アホ，あほ、ばか、バーカー、バカ、馬鹿野郎",
        "reading": "あほ、アホ、ばーかー、バカ、ばかやろう",
        "grammar": "形容詞",
        "ex1eng": "That shitbrains failed the test again.",
        "ex1jp": "あのバカはまたテスト落ちたよ",
        "level": 40,
        "fav": false
    },
    {
        "id": 105,
        "en": "shitbreath",
        "jp": "息臭い",
        "reading": "いきくさい",
        "grammar": "名詞",
        "ex1eng": "Buy a toothbrush, shitbreath.",
        "ex1jp": "息臭いお前！早く歯ブラシ買って来いよ！",
        "level": 40,
        "fav": false
    },
    {
        "id": 106,
        "en": "shitfaced",
        "jp": "酔っ払い、ベロベロ、泥酔",
        "reading": "よっぱらい、よっぱらてる、べろべろ、でいすい",
        "grammar": "形容詞",
        "ex1eng": "We were shitfaced last night.",
        "ex1jp": "俺らはクソ酔っ払ってた。",
        "level": 25,
        "fav": false
    },
    {
        "id": 107,
        "en": "shithole",
        "jp": "良くない場所、汚い場所",
        "reading": "きたない",
        "grammar": "名詞",
        "ex1eng": "The club was a shithole.",
        "ex1jp": "クラブはクソ悪かった。",
        "level": 25,
        "fav": false
    },
    {
        "id": 108,
        "en": "shithouse, shit house, shitter",
        "jp": "トイレ",
        "reading": "といれ、便所",
        "grammar": "名詞",
        "ex1eng": "I'm just going to the shithouse.",
        "ex1jp": "ちょっとトイレいってく",
        "grammar2": "形容詞",
        "ex2eng": "This club is shithouse.",
        "ex2jp": "このクラブはダサイ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 109,
        "en": "shitty",
        "jp": "ださい、つまらない、退屈、良くない、悪い、ボロボロ",
        "reading": "ダサイ、つまらん、つまらない、たいくつ、ぼろぼろ",
        "grammar": "形容詞",
        "ex1eng": "Haha, look at his shitty car!",
        "ex1jp": "あっはは！彼のボロボロ車見てよ！",
        "level": 10,
        "fav": false
    },
    {
        "id": 110,
        "en": "shiz, shiznit, the shit",
        "jp": "めっちゃ良い、良い、最高",
        "reading": "いい、さいこう、やばい、ヤバイ",
        "grammar": "形容詞",
        "ex1eng": "This track is the shit!",
        "ex1jp": "この曲はやばいぞ",
        "level": 1,
        "fav": false
    },
    {
        "id": 111,
        "en": "slut, slutbag, skank",
        "jp": "ふしだらな女、ヤリマン、尻軽女、サセ子",
        "reading": "やりマン",
        "grammar": "名詞",
        "ex1eng": "My ex is a slut.",
        "ex1jp": "俺の元カノはやりマンです。",
        "level": 35,
        "fav": false
    },
    {
        "id": 112,
        "en": "skullfuck",
        "jp": "ディープスロート",
        "reading": "でぃーぷすろーと",
        "grammar": "動詞",
        "ex1eng": "That pervert likes to skullfuck.",
        "ex1jp": "あのスケベはディープスロートのこと大好き。",
        "level": 30,
        "fav": false
    },
    {
        "id": 113,
        "en": "snatch",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "I need to shave my snatch.",
        "ex1jp": "あたしのまんこを剃らないと",
        "level": 15,
        "fav": false
    },
    {
        "id": 114,
        "en": "retard, tard",
        "jp": "アホ、知能の遅れた人",
        "reading": "あほ",
        "grammar": "名詞",
        "ex1eng": "You are a retard sometimes!",
        "ex1jp": "お前はたまに本当にアホだよ。",
        "level": 5,
        "fav": false
    },
    {
        "id": 115,
        "en": "testicles",
        "jp": "金玉、タマタマ",
        "reading": "きんたま、キンタマ、たまたま",
        "grammar": "名詞",
        "ex1eng": "My balls hurt like fuck!",
        "ex1jp": "きんたまがマジ痛え。",
        "level": 1,
        "fav": false
    },
    {
        "id": 116,
        "en": "tits, titties, breasts, melons",
        "jp": "おっぱい",
        "reading": "おっぱい",
        "grammar": "名詞",
        "ex1eng": "I want bigger tits.",
        "ex1jp": "もっと大きいおっぱい欲しいな。",
        "level": 1,
        "fav": false
    },
    {
        "id": 117,
        "en": "titfuck, tittyfuck",
        "jp": "パイズリ",
        "reading": "ぱいずり",
        "grammar": "動詞",
        "ex1eng": "I want to tittyfuck her someday.",
        "ex1jp": "いつか、彼女にパイズリしてもらいてぇ！",
        "level": 30,
        "fav": false
    },
    {
        "id": 118,
        "en": "twat",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "Her twat is shaven.",
        "ex1jp": "彼女はパイパン！",
        "level": 30,
        "fav": false
    },
    {
        "id": 119,
        "en": "va-j-j, vajayjay, vjayjay",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "My vajayjay is itchy.",
        "ex1jp": "あたしのまんこがかゆい",
        "level": 5,
        "fav": false
    },
    {
        "id": 120,
        "en": "unclefucker",
        "jp": "ゲイ、おかま",
        "reading": "げい、おかま、オカマ",
        "grammar": "名詞",
        "ex1eng": "Is he an unclefucker?",
        "ex1jp": "あいつはゲイなの？！",
        "level": 40,
        "fav": false
    },
    {
        "id": 121,
        "en": "vagina, vag",
        "jp": "まんこ、マンコ",
        "reading": "マンコ",
        "grammar": "名詞",
        "ex1eng": "My vajayjay is itchy.",
        "ex1jp": "あたしのまんこがかゆい",
        "level": 1,
        "fav": false
    },
    {
        "id": 122,
        "en": "whore, hussy",
        "jp": "ふしだらな女、ヤリマン、尻軽女、サセ子",
        "reading": "やりマン",
        "grammar": "名詞",
        "ex1eng": "My ex is a slut.",
        "ex1jp": "俺の元カノはやりマンです。",
        "level": 35,
        "fav": false
    }
];   
    
    localStorage["dictionaryData"] = JSON.stringify(dictionary);
  
  } else {
        console.log('Found a copy in localStorage, getting and converting');     
        var dictionary = JSON.parse(localStorage["dictionaryData"]); 
        console.log('Done. Set to localStorage and local variable. Available to use.');   
  }

  return {
    all: function() {
      return dictionary;
    },
    get: function(wordId) {
      return dictionary[wordId];
    },
    getFav: function(wordId){
      return dictionary[wordId].fav;
    },
    setFav: function(wordId,currentValue){
      if (currentValue){
        dictionary[wordId].fav = false;
        localStorage["dictionaryData"] = JSON.stringify(dictionary);
        console.log('set false on ' + dictionary[wordId].en + ' and updated localStorage');
        console.log('!CHECK! Fav value is: ' + dictionary[wordId].fav);    
      
      }else {
        dictionary[wordId].fav = true;
        localStorage["dictionaryData"] = JSON.stringify(dictionary);
        console.log('set true on ' + dictionary[wordId].en +' and updated localStorage');
        console.log('!CHECK! Fav value is: ' + dictionary[wordId].fav);    
        }   
    }
  }
})
.factory('Current', function() {
  
  var current = [];
  var currentCount = '';

  return {
    all: function() {
        return current;
    },
    count: function(){
        return currentCount;
    },
    updateCount: function(results){
        currentCount = results;
        return currentCount;
    }
  }
});

