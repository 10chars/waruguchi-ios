angular.module('starter.controllers', [])

.controller('SettingsCtrl', function($scope, Dictionary) {

})

.controller('HomeCtrl', function($scope, Dictionary, Current) {
  $scope.dictionary = Dictionary.all();
  $scope.save = Current.all();
  $scope.numOfResults = Current.count();
 

  $scope.results = function(searchTerm){
    
    cordova.plugins.Keyboard.close();
    if(searchTerm === ""){
      return;
    }

    $scope.save.length = 0; //Reset current search
    
    var st = searchTerm.toLowerCase();

    for(var i = 0; i <= $scope.dictionary.length-1 ; i++){
  		var check = $scope.dictionary[i].en;
      var checkJP = $scope.dictionary[i].jp;
      var checkReading = $scope.dictionary[i].reading;

  		if (check.indexOf(st) >=0 || checkJP.indexOf(st) >= 0 || checkReading.indexOf(st) >= 0){
        console.log('ADDED: ' + $scope.dictionary[i].jp);
  			$scope.save.push($scope.dictionary[i]);
  		}
  	}
    if($scope.save.length <= 0){ 
        $scope.numOfResults = 0;
        Current.updateCount(0);
    }else { 
        $scope.numOfResults = $scope.save.length;
        Current.updateCount($scope.save.length);
    }
  }
  
})

.controller('WordDetailCtrl', function($scope, $stateParams, Dictionary, Current, $ionicModal) {

  $scope.word = Dictionary.get($stateParams.wordId);
  var level = $scope.word.level;
  var remainder = 100-level;
  var levelColor = "";

  $scope.sendEmail = function(){
    var link = "mailto:?subject=Waruguchi&body=TEST";
    /*+"English: " + $scope.word.en + "Japanese: " + $scope.word.jp; */ 
    var url = encodeURI(link);
    console.log(url);
    var decoded = decodeURI(url);   
    $window.location.href = decoded;
  }

      if(level < 30){
        levelColor = "green";
      }else if(level > 30 && level < 70){
        levelColor = "yellow";
      }else levelColor = "red";

  $scope.myChartData = [{
                          value: level,
                          color: levelColor
                      },
                      {
                          value : remainder,
                          color : "lightgray"
                      }];


  
  if (localStorage.getItem("favoriteSavedData") === null) {
      console.log('Nothing in localStorage, making new array');    
      $scope.favorites = [];
      localStorage["favoriteSavedData"] = JSON.stringify($scope.favorites);
     
  } else {
      console.log('Found in localStorage, getting and converting');     
      $scope.favorites = JSON.parse(localStorage["favoriteSavedData"]); 
      console.log('Done. Set to scope variable.');   
  }

  $scope.favorited = Dictionary.getFav($stateParams.wordId);

  $scope.copyToClipboard = function(){
    cordova.plugins.clipboard.copy($scope.word.en);
    var x = $scope.word.en;
    alert(x);
  }

  $scope.favorite = function(){

      if(!$scope.favorited){   
        $scope.favorited = true;                        // Change state for button
        Dictionary.setFav($stateParams.wordId,false);   // Set the fav state in object
        $scope.favorites.push($scope.word);                  // Add it to favorites
        localStorage["favoriteSavedData"] = JSON.stringify($scope.favorites);    // Add it to localstorage              
        
    } else{
        
        $scope.favorited = false;                                 //set button to false.
        Dictionary.setFav($stateParams.wordId,true);              //set fav val to false.       
        var deleteThisId = $scope.word.id;
        
        console.log('Got id, removing ' + $scope.word.en);      

        for(var i = 0; i <= $scope.favorites.length-1; i++){      //find exact word, remove!
          if($scope.favorites[i].id === deleteThisId){
            $scope.favorites.splice(i, 1);  
          }
        }
        localStorage["favoriteSavedData"] = JSON.stringify($scope.favorites);  //update
      
    }
  
  }

  $ionicModal.fromTemplateUrl('templates/modal.html', function($ionicModal) {
        $scope.modal = $ionicModal;
    }, {
        scope: $scope,
        animation: 'slide-in-up'
    });

})　
.controller('FavoriteDetailCtrl', function($scope, $stateParams, Dictionary, Current) {

})
.controller('FavoritesCtrl', function($scope, $stateParams,　Dictionary, Current, $q, $ionicPopup) { 


    if (localStorage.getItem("favoriteSavedData") === null) {
      console.log('No Favorites in localStorage, show dummy array for now.');    
      $scope.favoriteListicle = [];  
      console.log('hide button.'); 
      $scope.isFavs = false;
      console.log('Done...button value: ' + $scope.isFavs.toString());    
     
  } else {
      console.log('Favorites found in localStorage, getting and converting');     
      $scope.favoriteListicle = [];
      $scope.favoriteListicle = JSON.parse(localStorage["favoriteSavedData"]);   
      if($scope.favoriteListicle.length >= 1){
      $scope.isFavs = true; 
      }
  }

      $scope.clearFavorites = function(){
      
            $ionicPopup.confirm({
              title: 'Delete Favorites',
              content: 'Are you sure?'
            }).then(function(res) {
              if(res) {
                console.log('You are sure');
                $scope.favoriteListicle = [];
                $scope.favoriteListicle = JSON.parse(localStorage["favoriteSavedData"]); 
                         
                  for(var i = 0; i <= $scope.favoriteListicle.length-1; i++){
                    console.log('loop :' + i);
                    var id = $scope.favoriteListicle[i].id;
                    Dictionary.setFav(id,true);
                  }
                  
                  localStorage.removeItem("favoriteSavedData");
                  console.log('removed from localStorage.');
                  location.reload();
                  $scope.isFavs = false;
           
              } else {
                console.log('You are not sure');
              }
            });
        
    }  

});
