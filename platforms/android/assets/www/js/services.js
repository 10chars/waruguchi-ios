angular.module('starter.services', [])


.factory('Dictionary', function() {
  

  if (localStorage.getItem("dictionaryData") === null) {
        
        console.log('Nothing in localStorage, making new dictionary array');    
    
    /*TODO: Read from json file.*/

        var dictionary = [
    {
        id: 0,
        en: "anus",
        jp: "ケツの穴",
        reading: "けつのあな",
        grammarex: "名詞",
        ex1eng: "My anus is sore.",
        ex1jp: "私のケツの穴が痛い。",
        level: 19,
        fav: false
    },
    {
        id: 1,
        en: "arse, ass",
        jp: "ケツ、おしり",
        reading: "けつ、おしり",
        grammar: "名詞",
        ex1eng: "You have a nice arse.",
        ex1jp: "君は良いケツがるよ。",
        level: 77,
        fav: false
    },
    {
        id: 2,
        en: "arsehole, asshole",
        jp: "ケツの穴、アホ",
        reading: "けつのあな、あほ",
        grammar: "名詞",
        ex1eng: "Fuck you, arsehole.",
        ex1jp: "死ね、アホよ！",
        level: 72,
        fav: false
    },
    {
        id: 3,
        en: "ass-hat, asshat",
        jp: "アホ",
        reading: "あほ",
        grammar: "名詞",
        ex1eng: "You are an ass-hat!",
        ex1jp: "お前はアホだよ！",
        level: 78,
        fav: false
    },
    {
        id: 4,
        en: "ass-pirate",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an ass-pirate.",
        ex1jp: "彼はゲイだよ！",
        level: 13,
        fav: false
    },
    {
        id: 5,
        en: "assbandit",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an assbandit.",
        ex1jp: "彼はゲイだよ！",
        level: 25,
        fav: false
    },
    {
        id: 6,
        en: "assbanger",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an assbanger.",
        ex1jp: "彼はゲイだよ！",
        level: 76,
        fav: false
    },
    {
        id: 7,
        en: "assfucker",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an assfucker.",
        ex1jp: "彼はゲイだよ！",
        level: 47,
        fav: false
    },
    {
        id: 8,
        en: "asshole, arsehole, asswipe",
        jp: "アホ、悪い人",
        reading: "あほ、わるいひと",
        grammar: "名詞",
        ex1eng: "He is an asshole.",
        ex1jp: "彼はアホだよ！",
        level: 84,
        fav: false
    },
    {
        id: 9,
        en: "asslick, asslicker, kiss ass",
        jp: "胡麻擂り",
        reading: "ごますり",
        grammar: "動詞",
        ex1eng: "I have to asslick at work.",
        ex1jp: "私は仕事で胡麻擂りをしないといけいない",
        level: 56,
        fav: false
    },
    {
        id: 10,
        en: "asslicker, ass kisser, kiss ass, kissass",
        jp: "胡麻擂りする人",
        reading: "ごますりするひと",
        grammar: "名詞",
        ex1eng: "He is an asslicker.",
        ex1jp: "彼は胡麻擂り人だよ！",
        level: 10,
        fav: false
    },
    {
        id: 11,
        en: "assmuncher",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an assmuncher.",
        ex1jp: "彼はゲイだよ！",
        level: 8,
        fav: false
    },
    {
        id: 12,
        en: "asspirate",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an ass pirate!",
        ex1jp: "彼がゲイだよ！",
        level: 99,
        fav: false
    },
    {
        id: 13,
        en: "axwound, pussy, cunt",
        jp: "まんこ、マンコ",
        reading: "まんこ",
        grammar: "名詞",
        ex1eng: "Look at that axewound!",
        ex1jp: "あのマンコ見てよ！",
        level: 72,
        fav: false
    },
    {
        id: 14,
        en: "bastard",
        jp: "糞垂れ、アホ (男)",
        reading: "くそたれ",
        grammar: "名詞",
        ex1eng: "My ex-boyfriend was a bastard.",
        ex1jp: "私の元彼はめっちゃ悪い人だった。",
        level: 52,
        fav: false
    },
    {
        id: 15,
        en: "bitch",
        jp: "女、あばずれ、やりマン、弱い男、文句する",
        reading: "おんな、あばずれ、やりまん、よわいおとこ",
        grammar: "名詞、動詞",
        ex1eng: "She is a bitch!",
        ex1jp: "彼女はあばずれだよ！",
        ex2eng: "Tom bitches everyday!",
        ex2jp: "トムは毎日文句がいっぱいあるよ！",
        level: 87,
        fav: false
    },
    {
        id: 16,
        en: "bitchy",
        jp: "文句が多い人",
        reading: "もんくがおおいひと",
        grammar: "形容詞",
        ex1eng: "She is bitchy!",
        ex1jp: "彼女は文句いっぱいあるタイプ！",
        level: 51,
        fav: false
    },
    {
        id: 17,
        en: "blow job, blowjob",
        jp: "フェラ、ふぇら",
        reading: "フェラ、ふぇら",
        grammar: "名詞、動詞",
        ex1eng: "I love a good blowjob!",
        ex1jp: "俺はフェラされるのが大好き！",
        ex2eng: "I gave Mike a blowjob last night.",
        ex2jp: "私は昨夜、マイクにフェラをしてあげました。",
        level: 06,
        fav: false
    },
    {
        id: 18,
        en: "bollocks, bollox, balls, nuts",
        jp: "金玉",
        reading: "きんたま",
        grammar: "名詞",
        ex1eng: "Lick my balls!",
        ex1jp: "金玉をなめろ",
        level: 61,
        fav: false
    },
    {
        id: 19,
        en: "boner",
        jp: "ギンギン、立ってる、エレクション",
        reading: "ギンギン、立ってる、エレクション",
        grammar: "名詞",
        ex1eng: "I have a boner.",
        ex1jp: "俺のちんちんが立ってるよ！",
        level: 26,
        fav: false
    },
    {
        id: 20,
        en: "bullshit",
        jp: "嘘、ウソ、悪い",
        reading: "うそ、わるい",
        grammar: "名詞、動詞、形容詞",
        ex1eng: "This club is bullshit.",
        ex1jp: "この店がマジ悪いよ！",
        ex2eng: "I bullshit at work everyday.",
        ex2jp: "俺は毎日仕事で嘘をつける！",
        level: 2,
        fav: false
    },
    {
        id: 21,
        en: "buttfucka, buttfucker",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "Are you a buttfucker?",
        ex1jp: "お前はおかまですか？",
        level: 65,
        fav: false
    },
    {
        id: 22,
        en: "camel toe, cameltoe",
        jp: "メコ筋",
        reading: "めこすじ",
        grammar: "名詞",
        ex1eng: "I can see your cameltoe!",
        ex1jp: "あなたのメコ筋を見えるよ！",
        level: 09,
        fav: false
    },
    {
        id: 23,
        en: "carpetmuncher, carpet muncher",
        jp: "レズ",
        reading: "れず",
        grammar: "名詞",
        ex1eng: "Yuki is a carpet muncher.",
        ex1jp: "ユキさんはレズです。",
        level: 96,
        fav: false
    },
    {
        id: 24,
        en: "clit, clitioris",
        jp: "くり、まめ、クリトリス",
        reading: "くり、まめ、クリトリス",
        grammar: "名詞",
        ex1eng: "You have a beautiful clit.",
        ex1jp: "君のクリは美しい。",
        level: 100,
        fav: false
    },
    {
        id: 25,
        en: "clusterfuck, cluster fuck",
        jp: "ダメの事、台無し",
        reading: "だめのこと、だいなし",
        grammar: "名詞、形容詞",
        ex1eng: "Yesterday was a cluster fuck",
        ex1jp: "昨日は台無しだった。",
        level: 41,
        fav: false
    },
    {
        id: 26,
        en: "cock",
        jp: "ちんこ、ちんぽ、ペニス、アホ",
        reading: "ちんこ、ちんぽ、ぺにす、あほ",
        grammar: "名詞",
        ex1eng: "Suck my cock.",
        ex1jp: "俺のちんぽをなめて",
        ex2eng: "He is a cock.",
        ex2jp: "彼はアホです。",
        level: 08,
        fav: false
    },
    {
        id: 27,
        en: "cocksmoker",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an cock smoker.",
        ex1jp: "そいつはゲイよ！",
        level: 15,
        fav: false
    },
    {
        id: 28,
        en: "cocksucker",
        jp: "ゲイ、おかま",
        reading: "げい、おかま、オカマ",
        grammar: "名詞",
        ex1eng: "He is an cocksucker.",
        ex1jp: "そいつはゲイよ！",
        level: 77,
        fav: false
    },
    {
        id: 29,
        en: "coochie, coochy, cooch, kooch, koochie",
        jp: "マンコ",
        reading: "マンコ、まんこ",
        grammar: "名詞",
        ex1eng: "Your cooch tastes good!",
        ex1jp: "君のマンコが美味しいよ！",
        level: 50,
        fav: false
    },
    {
        id: 30,
        en: "cum, semen",
        jp: "いく、イク、精子",
        reading: "いく、せいし",
        grammar: "名詞、動詞",
        ex1eng: "Drink all that cum.",
        ex1jp: "精子全飲みな",
        ex2eng: "Can I cum inside you?",
        ex2jp: "中に出しても良い？",
        level: 30,
        fav: false
    },
    {
        id: 31,
        en: "cumdumpster",
        jp: "売春婦、やりマン",
        reading: "ばいしゅんふ、やりまん",
        grammar: "名詞",
        ex1eng: "Yukiko is a cumdumster.",
        ex1jp: "ユキコちゃんはやりマンだよ！",
        level: 14,
        fav: false
    },
    {
        id: 32,
        en: "cunnilingus, eat out",
        jp: "クンニ、クンニリングス、ハーモニカ",
        reading: "クンニ、くんに",
        grammar: "名詞、動詞",
        ex1eng: "I love cunninglingus ♡.",
        ex1jp: "早くクンニしてくれ！",
        ex2eng: "Eat me out, now!",
        level: 29,
        fav: false
    }];
    
    localStorage["dictionaryData"] = JSON.stringify(dictionary);
  
  } else {
        console.log('Found a copy in localStorage, getting and converting');     
        var dictionary = JSON.parse(localStorage["dictionaryData"]); 
        console.log('Done. Set to localStorage and local variable. Available to use.');   
  }

  return {
    all: function() {
      return dictionary;
    },
    get: function(wordId) {
      return dictionary[wordId];
    },
    getFav: function(wordId){
      return dictionary[wordId].fav;
    },
    setFav: function(wordId,currentValue){
      if (currentValue){
        dictionary[wordId].fav = false;
        localStorage["dictionaryData"] = JSON.stringify(dictionary);
        console.log('set false on ' + dictionary[wordId].en + ' and updated localStorage');    
      
      }else {
        dictionary[wordId].fav = true;
        console.log('set true on ' + dictionary[wordId].en +' and updated localStorage');    
        }   
    }
  }
})
.factory('Current', function() {
  
  var current = [];
  var currentCount = '';

  return {
    all: function() {
        return current;
    },
    count: function(){
        return currentCount;
    },
    updateCount: function(results){
        currentCount = results;
        return currentCount;
    }
  }
});

